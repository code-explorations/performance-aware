/* Copyright (C) Tassio Naia 2023 */
/* First argument is source, second is destination */
#include <stdio.h>
#include <stdlib.h>

typedef unsigned char u8;


/* Bit patterns and masks */

/* dec |  bin | hex     dec |  bin | hex
 * ----+------+----     ----+------+----
 *  0  | 0000 |  0       8  | 1000 |  8
 *  1  | 0001 |  1       9  | 1001 |  9
 *  2  | 0010 |  2       10 | 1010 |  A
 *  3  | 0011 |  3       11 | 1011 |  B
 *  4  | 0100 |  4       12 | 1100 |  C
 *  5  | 0101 |  5       13 | 1101 |  D
 *  6  | 0110 |  6       14 | 1110 |  E
 *  7  | 0111 |  7       15 | 1111 |  F
 *
 */

#define mov_pattern 0x88        /* 1000 1000 */
#define mov_mask    0xFC        /* 1111 1100 */

#define d_bit_mask  0x02        /* 0000 0010 */
#define d_bit_shift (1)

#define w_bit_mask  0x01        /* 0000 0001 */
#define w_bit_shift (0)

#define mod_field_mask 0xC0     /* 1100 0000 */
#define mod_field_shift (6)

#define reg_field_mask 0x38     /* 0011 1000 */
#define reg_field_shift (3)

#define  rm_field_mask 0x07     /* 0000 0111 */
#define  rm_field_shift (0)

/* Reg = 010 W = 1 -> reg_field_encoding[2][1] (i.e., "DX") */
const char* reg_field_encoding[8][2] = {
    {"al","ax"},
    {"cl","cx"},
    {"dl","dx"},
    {"bl","bx"},
    {"ah","sp"},
    {"ch","bp"},
    {"dh","si"},
    {"bh","di"}
};

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr,
                "! I need two arguments (input file and output file).\n");
        exit(1);
    }

    printf( "Input file: %s\n"
            "Output file: %s\n",
            argv[1], argv[2]);

    FILE *input_file = NULL;
    /* Try to open input */
    if ((input_file = fopen(argv[1], "rb")) == NULL) {
        fprintf(stderr,
                "! Could not open input file.\n");
        exit(1);
    }

    FILE *output_file = NULL;
    /* Try to open output */
    if ((output_file = fopen(argv[2], "w")) == NULL) {
        fprintf(stderr,
                "! Could not open output file.\n");
        exit(1);
    }


    /* Print information about input file  */
    long input_file_size = 0;
    fseek(input_file, 0L, SEEK_END);
    input_file_size = ftell(input_file);
    printf("Input file size: %d bytes\n", input_file_size);
    rewind(input_file);


    /* Decode input file */
    const int buffer_size = 4 * 1024;
    u8 buffer[buffer_size];     /* For hw-001, must be even */
    int total_bytes_processed = 0;
    int input_length = 0;
    fprintf(output_file, "; %s:\n", argv[1]);
    fprintf(output_file, "bits 16\n");
    while ((input_length = fread(buffer, 1, buffer_size, input_file)) > 0) {
        fprintf(stderr,
                "Fetched %4d bytes (%d bytes processed so far):",
                input_length, total_bytes_processed);
        for (int i = 0; i < input_length; i+= 2) {
            unsigned int  first_bit = buffer[i + 0];
            unsigned int second_bit = buffer[i + 1];

            unsigned int dest_bit = ((first_bit & d_bit_mask) != 0);
            unsigned int wide_bit = ((first_bit & w_bit_mask) != 0);

            unsigned int mod_field = (second_bit & mod_field_mask) >> mod_field_shift;
            unsigned int reg_field = (second_bit & reg_field_mask) >> reg_field_shift;
            unsigned int  rm_field = (second_bit &  rm_field_mask) >>  rm_field_shift;

            printf (" %2x%2x -> %s,%d,%d,%d,%d,%d -> %s %s, %s\n",
                    first_bit, second_bit,
                    ((mov_mask & first_bit) == mov_pattern) ? "mov" : "???",
                    dest_bit, wide_bit,
                    mod_field, reg_field, rm_field,
                    ((mov_mask & first_bit) == mov_pattern) ? "mov" : "???",
                    dest_bit ? reg_field_encoding[reg_field][wide_bit] : reg_field_encoding[ rm_field][wide_bit],
                    dest_bit ? reg_field_encoding[ rm_field][wide_bit] : reg_field_encoding[reg_field][wide_bit]
                );
            char op[2][3] = { {0, 0, 0}, {0, 0, 0}  };

            /* fprintf(output_file, "mov %s, %s\n", op[dest_bit], op[1 -dest_bit]); */
            fprintf(output_file, "mov %s, %s\n",
                    dest_bit ? reg_field_encoding[reg_field][wide_bit] : reg_field_encoding[ rm_field][wide_bit],
                    dest_bit ? reg_field_encoding[ rm_field][wide_bit] : reg_field_encoding[reg_field][wide_bit]);
        }
        printf("\n");
        total_bytes_processed += input_length;
    }
    fclose(input_file);
    fclose(output_file);
    return 0;
}
